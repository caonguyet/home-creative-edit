let mainNav = document.getElementById("js-menu");
let navBarToggle = document.getElementById("js-navbar-toggle");

navBarToggle.addEventListener("click", function () {
  mainNav.classList.toggle("active");
});

//menu sticky

  window.onscroll = function () {
    stickyFunction();
  }

  let navbarbottom = document.getElementById ("navbarbottom");
  let sticky = navbarbottom.offsetTop;
  function stickyFunction(){
    if(window.pageYOffset){
      navbarbottom.classList.add("sticky")
    }
    else{
      navbarbottom.classList.remove("sticky")
    }
  }

$(document).ready(function () {
  $("#owl-slider").owlCarousel({
    navigation: true, // Show next and prev buttons
    slideSpeed: 300,
    paginationSpeed: 400,
    items : 1,

    responsive: {
        0: {
          items: 1,
        },
        600: {
          items: 1,
        },
        1000: {
          items: 1,
        },
      },
  });
});

// Scroll Top:

//get button

let scrollBtn = document.getElementById("scrolltop")

window.onscroll = function(){
  scroll();
  // console.log(1)
  // $(".number").counterUp({ delay: 5, time: 5000 });
}

//when scroll down 200px

function scroll() {
  let idFooter = document.getElementById('footer');
  if(window.pageYOffset + window.innerHeight >idFooter.offsetTop) {
    scrollBtn.style.display = "block";
  }else {
    scrollBtn.style.display = "none";
  }
}


function scrollToTop() {
  window.scrollTo({
    top:0,
    behavior: 'smooth'
  })
  
}

//add event click button

const $scrollButton = document.querySelector(".scrollbtnTop");
$scrollButton.addEventListener('click', ()=>{scrollToTop()});



